/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdd.c2.a4;

import javafx.scene.layout.AnchorPane;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Queue;
import javafx.scene.control.ColorPicker;
import javafx.scene.shape.Circle;

/**
 *
 * @author talob
 */
public class Circunferencia {
    int x1, y1;
    int x2, y2;
    int raio;
    
    public Circunferencia () {
        
    }
    public Circunferencia (int x1, int y1, int x2, int y2) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
    }
    
    public int getX1 () {
        return this.x1;
    } 
    public int getX2 () {
        return this.x2;
    }
    public int getY1 () {
        return this.y1;
    }
    public int getY2 () {
        return this.y2;
    }
    
    public void distanciaEuclidiana( int x1, int y1, int x2, int y2 ){
        int deltaXquadrado = (int)Math.pow( (x2-x1) , 2 );
        int deltaYquadrado = (int)Math.pow( (y2-y1) , 2 );
        
        //return (int)( Math.sqrt( deltaXquadrado + deltaYquadrado) );
        this.raio = (int)( Math.sqrt( deltaXquadrado + deltaYquadrado) );
    }
    
    public void bresenham(int x1, int y1, int x2, int y2, AnchorPane root, ColorPicker colorPicker, Double tamano, Queue<Circle> listCircle){
        distanciaEuclidiana(x1, y1, x2, y2 );
        
        int x = 0, y = raio, p = 3 - 2 * raio;

        plotaSimetricos(x, y, x1, y1, Color.BLACK.getRGB(), root, colorPicker, tamano, listCircle);
        while (x < y) { // 2 octante
            if (p < 0) {
                p += 4 * x + 6;
            } else {
                p += 4 * (x - y) + 10;
                y--;
            }
            x++;

            plotaSimetricos(x, y, x1, y1, Color.BLACK.getRGB(), root, colorPicker, tamano, listCircle);
        }
    }

    public void plotaSimetricos(int x, int y, int xCentro, int yCentro, int cor, AnchorPane root, ColorPicker colorPicker, Double tamano, Queue<Circle> listCircle) {
        
        
        //root.setRGB(xCentro + x, yCentro + y, cor);
        Circle circle = new Circle();
        circle.setFill(colorPicker.getValue());
        circle.setStroke(javafx.scene.paint.Color.TRANSPARENT);
        circle.setCenterX(xCentro + x);
        circle.setCenterY(yCentro + y);
        circle.setRadius(tamano);
        listCircle.add(circle);
        root.getChildren().addAll(circle);
        
        //root.setRGB(xCentro + x, yCentro - y, cor);
        circle = new Circle();
        circle.setFill(colorPicker.getValue());
        circle.setStroke(javafx.scene.paint.Color.TRANSPARENT);
        circle.setCenterX(xCentro + x);
        circle.setCenterY(yCentro - y);
        circle.setRadius(tamano);
        listCircle.add(circle);
        root.getChildren().addAll(circle);
        
        //root.setRGB(xCentro - x, yCentro + y, cor);
        circle = new Circle();
        circle.setFill(colorPicker.getValue());
        circle.setStroke(javafx.scene.paint.Color.TRANSPARENT);
        circle.setCenterX(xCentro - x);
        circle.setCenterY(yCentro + y);
        circle.setRadius(tamano);
        listCircle.add(circle);
        root.getChildren().addAll(circle);
        
        //root.setRGB(xCentro - x, yCentro - y, cor);
        circle = new Circle();
        circle.setFill(colorPicker.getValue());
        circle.setStroke(javafx.scene.paint.Color.TRANSPARENT);
        circle.setCenterX(xCentro - x);
        circle.setCenterY(yCentro - y);
        circle.setRadius(tamano);
        listCircle.add(circle);
        root.getChildren().addAll(circle);

        //root.setRGB(xCentro + y, yCentro + x, cor);
        circle = new Circle();
        circle.setFill(colorPicker.getValue());
        circle.setStroke(javafx.scene.paint.Color.TRANSPARENT);
        circle.setCenterX(xCentro + y);
        circle.setCenterY(yCentro + x);
        circle.setRadius(tamano);
        listCircle.add(circle);
        root.getChildren().addAll(circle);

        
        //root.setRGB(xCentro + y, yCentro - x, cor);
        circle = new Circle();
        circle.setFill(colorPicker.getValue());
        circle.setStroke(javafx.scene.paint.Color.TRANSPARENT);
        circle.setCenterX(xCentro + y);
        circle.setCenterY(yCentro - x);
        circle.setRadius(tamano);
        listCircle.add(circle);
        root.getChildren().addAll(circle);
        //root.setRGB(xCentro - y, yCentro + x, cor);
        circle = new Circle();
        circle.setFill(colorPicker.getValue());
        circle.setStroke(javafx.scene.paint.Color.TRANSPARENT);
        circle.setCenterX(xCentro - y);
        circle.setCenterY(yCentro + x);
        circle.setRadius(tamano);
        listCircle.add(circle);
        root.getChildren().addAll(circle);
        //root.setRGB(xCentro - y, yCentro - x, cor);
        circle = new Circle();
        circle.setFill(colorPicker.getValue());
        circle.setStroke(javafx.scene.paint.Color.TRANSPARENT);
        circle.setCenterX(xCentro - y);
        circle.setCenterY(yCentro - x);
        circle.setRadius(tamano);
        listCircle.add(circle);
        root.getChildren().addAll(circle);
      
    }
}
