package mdd.c2.a4;


import java.util.LinkedList;
import java.util.Queue;
import javafx.fxml.FXML;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Circle;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.AnchorPane;
import javafx.scene.control.ColorPicker;
import com.jfoenix.controls.JFXTextField;
import javafx.scene.paint.Color; 
import javafx.animation.FillTransition;
import javafx.scene.shape.Polygon;
import javafx.event.ActionEvent;
import javafx.util.Duration;  
public class FXMLDocumentController implements Initializable{
    private static boolean booleanRGB = false;
    @FXML
    private AnchorPane root;

    @FXML
    private Button buttonLine;

    @FXML
    private Button buttonBox;

    @FXML
    private Button menu;
    
    @FXML
    private CheckBox buttonRGB;
    
    @FXML
    private ColorPicker colorPciker;
    
    @FXML
    private JFXTextField size;
  
    @FXML
    private Button reset;
            
    Queue<Circle> listCircle = new LinkedList<Circle>();
    
    String opcion = "";
    
    double xincrement = 0;
    double yincrement = 0;
    Circle circle;
    
    int coord_x1 = 0;
    int coord_y1 = 0;
    
    int coord_x2 = 0;
    int coord_y2 = 0;
    
    int coord_last_x = 0;
    int coord_last_y = 0;
    
    int cont_clicks = 1;
    String color = new String();

    @FXML
    void OnMouseClickedReset(MouseEvent event) {
        root.getChildren().remove(1, root.getChildren().size());
    }
    @FXML
    void OnMouseClickedLine(MouseEvent event) {
    	opcion = "Trazo";
    	cont_clicks = 1;
    }
    @FXML
    void OnMouseClickedSquare(MouseEvent event) {
    	opcion = "Rectangulo";
        cont_clicks = 1;
    }
    @FXML
    void OnMouseClickedCircle(MouseEvent event) {
        opcion = "Circulo";
        cont_clicks = 1;
    }
    @FXML
    private void activateRGB(ActionEvent event) {
        
             if(booleanRGB==false){
                 booleanRGB = true;
                 //System.out.println("Hi");
                 //Polygon poly = new Polygon();
                 
                 for(Circle punto: listCircle){
                     //System.out.println(punto);
                     Circle circle = new Circle();
                    circle.setStroke(Color.TRANSPARENT);
                    circle.setCenterX(punto.getCenterX());
                    circle.setCenterY(punto.getCenterY());
                    circle.setRadius(punto.getRadius());
                    FillTransition fill = new FillTransition();
                    fill.setAutoReverse(true);
                    fill.setCycleCount(1000);
                    fill.setDuration(Duration.millis(1000)); 
                    fill.setFromValue(Color.PURPLE);
                    fill.setToValue(Color.YELLOW);
                    //fill.setFromValue(Color.YELLOW);
                    //fill.setToValue(Color.PINK);
                    fill.setShape(circle);
                    fill.play();
                    root.getChildren().addAll(circle);
                 }
                 
                 
                 
             }else{         
                booleanRGB=false;
                //System.out.println("No");
                 for(Circle punto: listCircle){
                     Circle circle = new Circle();
                        circle.setStroke(Color.TRANSPARENT);
                        circle.setCenterX(punto.getCenterX());
                        circle.setCenterY(punto.getCenterY());
                        circle.setRadius(punto.getRadius());
                        circle.setFill(Color.BLACK);
                        root.getChildren().addAll(circle);
                 }
             }
    }
    @FXML
    void OnMouseClickedPane(MouseEvent mouseEvent) {
    	//System.out.println(opcion);
    	if(opcion =="Rectangulo") {
    		
            
            if(cont_clicks == 1) {
            	coord_x1 =(int) mouseEvent.getX();
            	coord_y1 =(int) mouseEvent.getY();
            	cont_clicks = 2;
            }else if(cont_clicks == 2) {
            	coord_x2 =(int) mouseEvent.getX();
            	coord_y2 =(int) mouseEvent.getY();
            	
            	pintarRectangulo(coord_x1, coord_y1, coord_x2, coord_y2, root );
            	cont_clicks = 1;
            }
            
    	}else if(opcion == "Trazo"){

            if(cont_clicks == 1) {
            	coord_x1 =(int) mouseEvent.getX();
            	coord_y1 =(int) mouseEvent.getY();
            	cont_clicks = 2;
            }else if(cont_clicks == 2) {
            	coord_x2 =(int) mouseEvent.getX();
            	coord_y2 =(int) mouseEvent.getY();
            	pintarTrazo(coord_x1, coord_y1, coord_x2, coord_y2, root );
            	cont_clicks = 3;
            }else if (cont_clicks == 3) {
            	coord_last_x = coord_x2;
            	coord_last_y = coord_y2;
            	coord_x2 =(int) mouseEvent.getX();
            	coord_y2 =(int) mouseEvent.getY();
            	pintarTrazo(coord_last_x, coord_last_y, coord_x2, coord_y2, root );
            	coord_last_x = coord_x2;
            	coord_last_y = coord_y2;
            }
    	}else{
            if(cont_clicks == 1) {
            	coord_x1 =(int) mouseEvent.getX();
            	coord_y1 =(int) mouseEvent.getY();
            	cont_clicks = 2;
            }else if(cont_clicks == 2) {
            	coord_x2 =(int) mouseEvent.getX();
            	coord_y2 =(int) mouseEvent.getY();
            	Circunferencia circulo = new Circunferencia();
                double tamano2 = Double.parseDouble(size.getText());
                circulo.bresenham(coord_x1, coord_y1, coord_x2, coord_y2, root, colorPciker, tamano2, listCircle);
   
            	cont_clicks = 1;
            }
        }
    }
    
    @FXML
    void OnKeyReleased(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER){
			//System.out.println("Enter");
			pintarTrazo(coord_last_x, coord_last_y, coord_x1, coord_y1, root );
		}
    }
 
    
    public void pintarTrazo(int coord_x1, int coord_y1, int coord_x2, int coord_y2,AnchorPane root ){
    	//System.out.println("X1: "+coord_x1+"\tY1: "+coord_y1+"\nX2: "+coord_x2+"\tY2: "+coord_y2);
    	int distancia_x = Math.abs(coord_x2 - coord_x1);
    	int distancia_y = Math.abs(coord_y2 - coord_y1);
    	double x = coord_x1;
    	double y = coord_y1;
    	double proporcion_x = 0;
    	double proporcion_y = 0;
    	String tamano=size.getText();
        double tamano2 = Double.parseDouble(tamano);
        //System.out.println("Sentado debajo un mango, debajo de un arbol de mango");
    	if(distancia_x > distancia_y) {
    		proporcion_x = (double)(coord_x2 - coord_x1) / (double)distancia_x;
    		proporcion_y = (double)(coord_y2 - coord_y1) / (double)distancia_x;
    		for(int i = 0; i < distancia_x; i++){
                circle = new Circle();
                circle.setFill(colorPciker.getValue());
                x += proporcion_x;
                y += proporcion_y;
                //System.out.println(x+":"+y);
                circle.setStroke(Color.TRANSPARENT);
                circle.setCenterX(x);
                circle.setCenterY(y);
                circle.setRadius(tamano2);
                listCircle.add(circle);
                root.getChildren().addAll(circle);
            }
    	}else {
    		proporcion_x = (double) (coord_x2 - coord_x1) / (double)distancia_y;
    		proporcion_y = (double) (coord_y2 - coord_y1) / (double)distancia_y;
    		for(int i = 0; i < distancia_y; i++){
                circle = new Circle();
                circle.setFill(colorPciker.getValue());
                x += proporcion_x;
                y += proporcion_y;
                //System.out.println(x+":"+y);
                circle.setStroke(Color.TRANSPARENT);
                circle.setCenterX(x);
                circle.setCenterY(y);
                circle.setRadius(tamano2);
                listCircle.add(circle);
                root.getChildren().addAll(circle);
            }
    	}
    }
    
    public void pintarRectangulo(int coord_x1, int coord_y1, int coord_x2, int coord_y2,AnchorPane root ){
    	//System.out.println("X1: "+coord_x1+"\tY1: "+coord_y1+"\nX2: "+coord_x2+"\tY2: "+coord_y2);
    	int distancia_x = Math.abs(coord_x2 - coord_x1);
    	int distancia_y = Math.abs(coord_y2 - coord_y1);
    	int x = 0;
    	int y = 0;
        if(coord_x1 > coord_x2) {
                x = coord_x2;
        }else {
                x = coord_x1;
        }
        if(coord_y1 < coord_y2) {
                y = coord_y1;
        }else {
                y = coord_y2;
        }
        //System.out.println("DistX: "+distancia_x+"\tDistY: "+distancia_y);
        String tamano=size.getText();
        double tamano2 = Double.parseDouble(tamano);
            for(int i = x; i < (distancia_x + x) ; i++){
            circle = new Circle();
            circle.setFill(colorPciker.getValue());
            circle.setStroke(Color.TRANSPARENT);
            circle.setCenterX(i);
            circle.setCenterY(y);
            circle.setRadius(tamano2);
            listCircle.add(circle);
            root.getChildren().addAll(circle);

            circle = new Circle();
            circle.setFill(colorPciker.getValue());
            circle.setStroke(Color.TRANSPARENT);
            circle.setCenterX(i);
            circle.setCenterY(y+distancia_y);
            circle.setRadius(tamano2);
            listCircle.add(circle);
            root.getChildren().addAll(circle);
        }
    		

        for(int i = y; i < (distancia_y + y); i++){
            circle = new Circle();
            circle.setFill(colorPciker.getValue());
            circle.setCenterX(x);
            circle.setCenterY(i);
            circle.setRadius(tamano2);
            listCircle.add(circle);
            root.getChildren().addAll(circle);

            circle = new Circle();
            circle.setFill(colorPciker.getValue());
            circle.setCenterX(x+distancia_x);
            circle.setCenterY(i);
            circle.setRadius(tamano2);
            listCircle.add(circle);
            root.getChildren().addAll(circle);
        }
    }
    void ellipse(int xm, int ym, int a, int b){
        int dx = 0, dy = b; /* im I. Quadranten von links oben nach rechts unten */
        long a2 = a*a, b2 = b*b;
        long err = b2-(2*b-1)*a2, e2; /* Fehler im 1. Schritt */
        double tamano2 = Double.parseDouble(size.getText());
        Circle circle2 = new Circle();
        Circle circle3 = new Circle();
        Circle circle4 = new Circle();
        do {
            circle = new Circle();
            circle.setFill(colorPciker.getValue());
            circle.setStroke(Color.TRANSPARENT);
            circle.setCenterX(xm+dx);
            circle.setCenterY(ym+dy);
            circle.setRadius(tamano2);
            listCircle.add(circle);
            root.getChildren().addAll(circle);
            
            //setPixel(xm+dx, ym+dy); /* I. Quadrant */
            circle2 = new Circle();
            circle2.setFill(colorPciker.getValue());
            circle2.setStroke(Color.TRANSPARENT);
            circle2.setCenterX(xm-dx);
            circle2.setCenterY(ym+dy);
            circle2.setRadius(tamano2);
            listCircle.add(circle2);
            root.getChildren().addAll(circle2);
            //setPixel(xm-dx, ym+dy); /* II. Quadrant */
            circle3 = new Circle();
            circle3.setFill(colorPciker.getValue());
            circle3.setStroke(Color.TRANSPARENT);
            circle3.setCenterX(xm-dx);
            circle3.setCenterY(ym-dy);
            circle3.setRadius(tamano2);
            listCircle.add(circle3);
            root.getChildren().addAll(circle3);
            //setPixel(xm-dx, ym-dy); /* III. Quadrant */
            circle4 = new Circle();
            circle4.setFill(colorPciker.getValue());
            circle4.setStroke(Color.TRANSPARENT);
            circle4.setCenterX(xm+dx);
            circle4.setCenterY(ym-dy);
            circle4.setRadius(tamano2);
            listCircle.add(circle4);
            root.getChildren().addAll(circle4);
            //setPixel(xm+dx, ym-dy); /* IV. Quadrant */
            
            e2 = 2*err;
            if (e2 <  (2*dx+1)*b2) { dx++; err += (2*dx+1)*b2; }
            if (e2 > -(2*dy-1)*a2) { dy--; err -= (2*dy-1)*a2; }
        } while (dy >= 0);

        
     }
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		
	}
    
    
}

