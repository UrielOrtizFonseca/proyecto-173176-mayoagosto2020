# **Configuración Software**

## JDK
* [1.8.0_251](https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html)
## JRE
* [1.8.0_251](https://www.oracle.com/java/technologies/javase-jre8-downloads.html)
---
## Scene builder
* 8.5.0

## NetBeans.
* [8.2](https://netbeans.org/downloads/8.2/rc/)
---
## .JAR

## Fontawesomefx
* [8.9](https://jar-download.com/artifacts/de.jensd/fontawesomefx/8.9/source-code)

## JFoenix
* [8.0.7](https://jar-download.com/artifacts/com.jfoenix/jfoenix/8.0.7/source-code)

### Referencias:
* Importación de los .JAR

https://stackoverflow.com/questions/48841043/importing-jfoenix-library-to-scenebuilder-javafx

* Design

https://www.youtube.com/watch?v=ZVtys3GgkMo