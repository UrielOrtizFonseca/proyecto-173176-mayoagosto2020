/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multimediaact3;

import java.util.Scanner;

/**
 *
 * @author Ortiz
 */
public class MultimediaAct3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String end = "";
        String matriz [][];
        
        do{
        System.out.println("Escribe un numero Impar: ");
            int number = sc.nextInt();
            double temp = 0;
            temp = Math.ceil(number / 2);
            
            if(number%2==0){
                System.out.println(number+"  Es un numero Par");
                System.out.println("*Debe escribir un número Impar*");
            }else{
                if(number==1){
                    System.out.println("_______________ Tabla 1 _______________");
                    System.out.println("X");
                    System.out.println("_______________ Tabla 2 _______________");
                    System.out.println("X");
                }else{
                    System.out.println(number+"  Es un numero Impar");
                    matriz = new String[number][number];
                    for(int i=0;i<number;i++){
                        for(int j=0;j<number;j++){
                            matriz[i][j] = "-";
                        }
                    }
                    System.out.println("_______________ Tabla 1 _______________");
                    int temp1Up =(int) temp; 
                    int temp1Up2 =(int) temp;
                    int temp1Down =number-1; 
                    int temp1Down2 =0;
                    int contador = number-1;
                    for(int i=0;i<number;i++){
                        for(int j=0;j<number;j++){
                            if(i>=temp && (j>=temp1Down2 && j<=temp1Down)){
                                matriz[i][j] = "X";
                                if(j==contador){
                                    temp1Down = temp1Down-1;
                                    temp1Down2 = temp1Down2+1;
                                    contador=contador-1;
                                }
                            }
                            if(j>=temp1Up2 && j<=temp1Up && i<=temp){
                                 matriz[i][j] = "X";
                            }
                            System.out.print(" "+matriz[i][j]+" ");
                        }
                        if(temp1Up<number-1){
                           temp1Up = temp1Up+1;
                           temp1Up2 = temp1Up2-1; 
                        }
                        System.out.println();
                    }
                    for(int i=0;i<number;i++){
                        for(int j=0;j<number;j++){
                            matriz[i][j] = "-";
                        }
                    }
                    System.out.println("_______________ Tabla 2 _______________");
                    int tempUp =(int) temp; 
                    int tempUp2 =(int)temp;
                    int tempDown =number-1; 
                    int tempDown2 =0;
                    boolean bandera =false;
                    for(int i=0;i<number;i++){
                        for(int j=0;j<number;j++){
                            if(i==0 || i==number-1){
                                matriz[i][(int)temp] = "X";
                            }
                            if(i>0&& tempUp2>0 && tempUp<number && bandera==false){
                                tempUp = tempUp +1;
                                tempUp2 = tempUp2 -1;
                                matriz[i][tempUp] = "X";
                                matriz[i][tempUp2] = "X";
                                bandera = true;
                            }
                            if(i>=temp && tempDown2>=0 && tempDown<=number && bandera==false){
                                tempDown = tempDown -1;
                                tempDown2 = tempDown2 +1;
                                matriz[i][tempDown] = "X";
                                matriz[i][tempDown2] = "X";
                                bandera = true;
                            }
                            System.out.print(" "+matriz[i][j]+" ");
                        }
                        bandera=false;
                        System.out.println();
                    }
                }
            }
            System.out.println("Deseas continuar?   Si o No");
            end = sc.next();
        }while(end.equals("Si"));
    }
}