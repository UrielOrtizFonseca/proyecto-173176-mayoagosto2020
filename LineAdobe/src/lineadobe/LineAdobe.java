/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lineadobe;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.scene.shape.Circle;
import java.util.ArrayList;
import java.util.Stack;
import javafx.scene.layout.Pane;
/**
 *
 * @author Ortiz
 */
public class LineAdobe extends Application {
    public static ArrayList<Circle> listCircle = new ArrayList<Circle>();
    int contador = 0;
    int length = 0;
    double xincrement = 0;
    double yincrement = 0;
    Circle circle;
    @Override
    public void start(Stage primaryStage) {
        Group estados = new Group();
        Pane root = new Pane();
        root.getChildren().add(estados);
        Scene scene = new Scene(root, 700, 300);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Line");
        primaryStage.show();
        
        
        ArrayList<Circle> listCircle = new ArrayList<Circle>();
        Stack stackX = new Stack();
        Stack stackY = new Stack();
        
        // Detectar clic en ratón (pulsado y soltado)
        scene.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                circle = new Circle();
                if(mouseEvent.getButton() == MouseButton.PRIMARY) {
                    //System.out.println("Botón principal");
                }
                stackX.push(mouseEvent.getX());
                stackY.push(mouseEvent.getY());
                    if(stackX.size()==2){
                        
                        double x2 = (double) stackX.peek();
                        double y2 = (double) stackY.peek();
                        stackX.pop();
                        stackY.pop();
                        double x = (double) stackX.peek();
                        double y = (double) stackY.peek();
                        stackX.pop();
                        stackY.pop();
                        stackX.push(x2);
                        stackY.push(y2);
                        
                        length = (int)Math.abs(x2 - x);
                        if (Math.abs (y2 - y) > length){
                            length = (int) Math.abs (y2 - y);
                        }
                        xincrement = (double) (x2 - x) / (double) length;
                        yincrement = (double) (y2 - y) / (double) length;
                        x = x + 1;
                        y = y + 1;
                        for(int i = 1; i <= length; ++i){
                            circle = new Circle();
                            x = x + xincrement;
                            y = y + yincrement;
                            circle.setCenterX(x);
                            circle.setCenterY(y);
                            circle.setRadius(2.0f);
                            estados.getChildren().addAll(circle);
                            //Lo de abajo es del arrayList
                            //listCircle.add(circle);
                        }
                        
                    }
                //System.out.println("Contador: "+contador);

                //System.out.println("FIRST ___ X: "+x+ "  Y: "+y);
                //root.getChildren().addAll(listCircle.get(contador));
                //System.out.println("Array: "+listCircle);
                /*
                //Esto es para el arrayList
                int temp = listCircle.size() - contador;
                for(int j =contador;j<temp;j++){
                    estados.getChildren().addAll(listCircle.get(j));
                }
                System.out.println("Array: "+listCircle.size());
                contador=listCircle.size();
                */
                //root.getChildren().addAll(circle); 
            }
        });
        
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
